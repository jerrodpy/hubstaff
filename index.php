<?php
require_once __DIR__ . '/Hubstaff.php';


$hs = new Hubstaff();

// If there is no token in the session, we redirect to authorization,
// otherwise we pull the token from the session
$hs->authorization();


// получение иноформации о зарегистрированом пользователе
$info = $hs->getMyInfo();


echo '<pre>';
print_r($info);
echo '</pre>';
