<?php

require_once __DIR__ . '/Hubstaff.php';

$hs = new Hubstaff();

if (isset($_GET["code"])) {
    $hs->generateAccessToken($_GET["code"]);
    print_r('Successfully logged in, authorization token: '. $hs->getAccessToken());
}


//echo '<pre>';
//print_r($_REQUEST);
//echo '</pre>';