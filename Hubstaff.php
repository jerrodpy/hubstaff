<?php

/**
 * Class Hubstaff
 */
class Hubstaff
{
    private $baseUrl;
    private $clientId;
    private $clientSecret;

    private $redirectUrl;
    private $accessToken;
    private $responseCode;

    /**
     * hubstaff constructor.
     */
    public function __construct()
    {
        require_once __DIR__ . '/config.php';

        $this->baseUrl = BASE_URL;
        $this->clientId = CLIENT_ID;
        $this->clientSecret = CLIENT_SECRET;
        $this->redirectUrl = REDIRECT_URL;
        $this->startSession();
        $this->getConfig();




    }


    private function getConfig()
    {
        if (!$this->getSessionKey('config')) {
            $config = $this->fetchURL(CONFIG_URL);
            $this->setSessionKey('config', json_decode($config, true));
        }
    }

    /**
     * Used for arbitrary value generation for nonces and state
     *
     * @return string
     */
    private function generateRandString(): string
    {
        return md5(uniqid(rand(), TRUE));
    }

    /**
     * @param string $url
     */
    private function redirect(string $url)
    {
        header('Location: ' . $url);
        exit;
    }

    /**
     * Use session to manage a nonce
     */
    private function startSession(): void
    {
        if (!isset($_SESSION)) {
            @session_start();
        }
    }


    /**
     * @param string $key
     *
     * @return string|null
     */
    private function getSessionKey(string $key)
    {
        $this->startSession();

        return $_SESSION[$key] ?? null;
    }

    /**
     * @param string $key
     * @param $value
     */
    private function setSessionKey(string $key, $value): void
    {
        $this->startSession();

        $_SESSION[$key] = $value;
    }

    /**
     * @param string      $url
     * @param string|null $post_body
     * @param array       $headers
     *
     * @return string
     * @throws Exception
     */
    private function fetchURL(string $url, string $post_body = null, array $headers = []): string
    {
        if (strpos($url, 'http') === false) {
            $url = $this->baseUrl . $url;
        }

        $ch = curl_init();

        // Determine whether this is a GET or POST
        if ($post_body !== null) {

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_body);

            // Default content type is form encoded
            $content_type = 'application/x-www-form-urlencoded';

            // Determine if this is a JSON payload and add the appropriate content type
            if (is_object(json_decode($post_body))) {
                $content_type = 'application/json';
            }

            // Add POST-specific headers
            $headers[] = "Content-Type: {$content_type}";
            $headers[] = 'Content-Length: ' . strlen($post_body);

        }

        // If we set some headers include them
        if (count($headers) > 0) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);

        $info = curl_getinfo($ch);
        $this->responseCode = $info['http_code'];

        if ($output === false) {
            throw new Exception('Curl error: ' . curl_error($ch));
        }

        curl_close($ch);

        return $output;
    }


    /**
     * @param array $tokens
     *
     * @throws Exception
     */
    private function saveToken(array $tokens): void
    {
        foreach ($tokens as $tokenKey => $tokenValue) {

            $this->setSessionKey($tokenKey, $tokenValue);

            if ('expires_in' === $tokenKey) {
                $date = new \DateTime();
                $expires_in = $date->add(new \DateInterval(sprintf('PT%sS', $tokenValue)));
                $this->setSessionKey('expires_in', $expires_in);
            }

            if ('access_token' === $tokenKey) {
                $this->accessToken = $tokenValue;
            }
        }

    }

    /**
     * @throws Exception
     */
    private function checkToken(): void
    {
        if ($this->getSessionKey('access_token') && $this->getSessionKey('expires_in') > new \DateTime()) {

            $this->accessToken = $this->getSessionKey('access_token');

            return;
        }

        $this->refreshTokens();
    }

    /**
     * @throws Exception
     */
    private function refreshTokens(): void
    {
        $config = $this->getSessionKey('config');

        if (!isset($config['token_endpoint'])) {
            throw new Exception('The configuration object was not found.');
        }

        $params = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->getSessionKey('refresh_token'),
            'scope' => 'openid profile email'
        ];

        $headers = ['Authorization: Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret)];

        $postParams = http_build_query($params, null, '&');

        $response = $this->fetchURL($config['token_endpoint'], $postParams, $headers);

        try {
            $tokens = json_decode($response);

            $this->saveToken($tokens, true);

        } catch (Exception $e) {
            throw new Exception('Error getting token');
        }
    }

    /**
     * @return string|null
     */
    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    /**
     * @return bool|string
     * @throws Exception
     */
    public function getMyInfo(): string
    {

        $this->checkToken();

        $headers = ['Authorization: Bearer ' . $this->accessToken];

//        return $this->fetchURL('/v2/users/me', null, $headers);
        return $this->fetchURL('/v1/users', null, $headers);
//        return $this->fetchURL('/v1/organizations', null, $headers);
    }

    /**
     * @throws Exception
     */
    public function authorization(): void
    {
        // if already authorized, check the token and exit
        if ($this->getSessionKey('refresh_token')) {
            $this->checkToken();
            return;
        }

        $config = $this->getSessionKey('config');

        if (!isset($config['authorization_endpoint'])) {
            throw new Exception('The configuration object was not found.');
        }

        $nonce = $this->generateRandString();

        $params = [
            'client_id' => $this->clientId,
            'response_type' => 'code',
            'redirect_uri' => $this->redirectUrl,
            'nonce' => $nonce,
            'state' => $nonce,
            'scope' => 'openid profile email'
        ];

        $postParams = http_build_query($params, null, '&', PHP_QUERY_RFC3986);

        $urlAuth = $config['authorization_endpoint'] . '?' . $postParams;

        $this->redirect($urlAuth);
    }

    /**
     * @param string $code
     *
     * @throws Exception
     */
    public function generateAccessToken(string $code): void
    {
        if($this->getSessionKey('access_token')){
            $this->accessToken = $this->getSessionKey('access_token');
           return ;
        }

        $config = $this->getSessionKey('config');

        if (!isset($config['token_endpoint'])) {
            throw new Exception('The configuration object was not found.');
        }

        $params = [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => $this->redirectUrl,
        ];

        $headers = ['Authorization: Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret)];

        $postParams = http_build_query($params, null, '&');

        $response = $this->fetchURL($config['token_endpoint'], $postParams, $headers);

        try {
            $tokens = json_decode($response, true);

            $this->saveToken($tokens);

        } catch (Exception $e) {
            throw new Exception('Error getting token');
        }

    }


}